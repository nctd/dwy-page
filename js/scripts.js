(function() {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('text-center border border-light p-5');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();


function resetLogin() {
    document.getElementById('form-nav').reset();
    document.getElementById('labelEmail').className = '';
    document.getElementById('labelPass').className = '';
}

function resetRegistro() {
    document.getElementById('form-reg').reset();
    document.getElementById('labelNombre').className = '';
    document.getElementById('labelApellido').className = '';
    document.getElementById('labelCorreo').className = '';
    document.getElementById('labelContr').className = '';
    document.getElementById('labelTelef').className = '';
}

function resetLost() {
    document.getElementById('modalLoginForm').modal('toggle');
    document.getElementById('labelLost').className = '';
}

/* function hideLogin(){
    x = document.getElementById('form-nav');
    if(x.style.display === "none"){
        x.style.display = "block";
    }else{
        x.style.display = "none";
    }
    
} */
/* function showLogin(){
    x = document.getElementById('form-nav');
    if(x.style.display === "block"){
        x.style.display = "none";
    }else{
        x.style.display = "block";
    }
} */

function isNumericKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 &&
        (charCode < 48 || charCode > 57))
        return true;
    return false;
}


function numbersonly(e) {
    var unicode = e.charCode ? e.charCode : e.keyCode
    if (unicode != 8) { //if the key isn't the backspace key (which we should allow)
        if (unicode < 48 || unicode > 57) //if not a number
            return false //disable key press
    }
}

$('.post-wrapper').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 2000,
    nextArrow: $('.next'),
    prevArrow: $('.prev'),
    responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});

/* var arrLang = new Array();
arrLang['en'] = new Array();

arrLang['en']['inicio'] = 'Home';
arrLang['en']['iniciar'] = 'Log In';
arrLang['en']['registrar'] = 'Sign Up';

$(document).ready(function(e) {
    $('.translate').click(function() {
        var lang = $(this).attr('id');

        $('#lang').each(function(index, element) {
            $(this).text(arrLang[lang][$(this).attr('key')]);
        });
    });
}); */